package kr.go.korean.corpus;

import kr.go.korean.corpus.parser.entity.User;
import kr.go.korean.corpus.parser.service.DocumentService;
import kr.go.korean.corpus.parser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ParserApplication {

    @Autowired
    DocumentService documentService;

    @Autowired
    UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(ParserApplication.class, args);
    }

    @Bean
    CommandLineRunner runnerUser() {
        return args -> {

            if(userService.getAll().size() <= 0){
                userService.add(User.builder()
                        .userName("admin")
                        .password("$2a$10$TgB3mIBmfA34HxqIr0qMBOB8YoJO.sB2tIg/6P/1whOBcJT8O00Ge")
                        .realName("관리자")
                        .resetPassword(true)
                        .build());

                userService.add(User.builder()
                        .userName("user1")
                        .password("$2a$10$TgB3mIBmfA34HxqIr0qMBOB8YoJO.sB2tIg/6P/1whOBcJT8O00Ge")
                        .realName("사용자1")
                        .resetPassword(true)
                        .build());
            }

        };
    }
}
