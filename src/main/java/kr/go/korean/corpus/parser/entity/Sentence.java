package kr.go.korean.corpus.parser.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(toBuilder = true)
@Entity
@Table(name="tb_sentence")
@SequenceGenerator(
        name="seq_sentence",
        sequenceName="seq_sentence",
        allocationSize=1
)
public class Sentence {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sentence")
    @Column(name = "sentence_id")
    private Long id;

    @Column(name="sort", length = 50)
    private String sort;

    @Column(name="content", length = 4000)
    private String content;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sentence")
    private List<Ne> nes;

    @ManyToOne
    @JoinColumn(name = "document_id")
    private Document document;
}
