package kr.go.korean.corpus.parser.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(toBuilder = true)
@Entity
@Table(name="tb_cr")
@SequenceGenerator(
        name="seq_cr",
        sequenceName="seq_cr",
        allocationSize=1
)
public class Cr {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_cr")
    @Column(name = "cr_id")
    private Long id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cr")
    private List<CrEntity> crEntities;

    @ManyToOne
    @JoinColumn(name = "document_id")
    private Document document;

}
