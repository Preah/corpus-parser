package kr.go.korean.corpus.parser.repository;

import kr.go.korean.corpus.parser.entity.Sentence;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SentenceRepository extends JpaRepository <Sentence, Long> {

}
