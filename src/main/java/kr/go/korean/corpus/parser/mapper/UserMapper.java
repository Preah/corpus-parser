package kr.go.korean.corpus.parser.mapper;


import kr.go.korean.corpus.parser.entity.User;
import kr.go.korean.corpus.parser.form.UserForm.Request;
import kr.go.korean.corpus.parser.form.UserForm.Response;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(imports = {LocalDate.class, DateTimeFormatter.class})
public interface UserMapper {

    UserMapper mapper = Mappers.getMapper(UserMapper.class);

    User toUser(Request.UserAdd form);

    List<Response.FindAll> toFindAll(List<User> entities);

    @Mappings({
            @Mapping(target = "completeCount", source = "entity", qualifiedByName = "toCompleteCount"),
            @Mapping(target = "progressCount", source = "entity", qualifiedByName = "toProgressCount")
    })
    Response.FindAll toFindAll(User entity);



    @Named("toCompleteCount")
    default Integer toCompleteCount(User user){
        return user.getDocuments().stream().filter(document -> document.getStatus().equals(true)).collect(Collectors.toList()).size();
    }

    @Named("toProgressCount")
    default Integer toProgressCount(User user){
        return user.getDocuments().stream().filter(document -> document.getStatus().equals(false)).collect(Collectors.toList()).size();
    }
}

