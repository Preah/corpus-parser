package kr.go.korean.corpus.parser.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.go.korean.corpus.parser.code.Status;
import kr.go.korean.corpus.parser.entity.Cr;
import kr.go.korean.corpus.parser.form.DocumentForm;
import kr.go.korean.corpus.parser.form.DocumentForm.Request;
import kr.go.korean.corpus.parser.mapper.CrContext;
import kr.go.korean.corpus.parser.mapper.CrMapper;
import kr.go.korean.corpus.parser.mapper.DocumentMapper;
import kr.go.korean.corpus.parser.predicate.DocumentPredicate;
import kr.go.korean.corpus.parser.service.CrService;
import kr.go.korean.corpus.parser.service.DocumentService;
import kr.go.korean.corpus.parser.service.SentenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
public class DocumentController {

    @Autowired
    DocumentService documentService;

    @Autowired
    SentenceService sentenceService;

    @Autowired
    CrService crService;

    @GetMapping("/document/list")
    public String documentPage(Model model, @Valid Request.FindAll findAll, @PageableDefault(size = 10) Pageable pageable) {

        model.addAttribute("findAll", findAll);
        model.addAttribute("documentPage", documentService.getPage(DocumentPredicate.getPage(findAll), pageable).map(DocumentMapper.mapper::toFindAll));
        return "/document/document-list";
    }


    @GetMapping("/document/detail")
    public String documentDetail(Model model, @RequestParam("id") Long documentId) {

        model.addAttribute("document", DocumentMapper.mapper.toFindOne(documentService.get(documentId)));
        return "/document/document-detail";
    }


    @PostMapping("/document/reg")
    public String documentReg(@RequestParam("json") String json, @RequestParam("documentId") Long documentId, @RequestParam("status") Boolean status) throws IOException {

        ObjectMapper jsonMapper = new ObjectMapper().configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        TypeReference<Request.CrConvert> typeReference = new TypeReference<Request.CrConvert>(){};

        Request.CrConvert convert = jsonMapper.readValue(json, typeReference);
        List<Cr> crs = CrMapper.mapper.toCrContext(CrContext.builder().build(), convert.getCrs());
        crService.addList(crs, documentId, status);

        return "redirect:/document/list";
    }

    @GetMapping("/document/detail/json")
    @ResponseBody
    public String documentJson(@RequestParam("documentId") Long documentId) throws JsonProcessingException {

        ObjectMapper jsonMapper = new ObjectMapper();
        return jsonMapper.writeValueAsString(DocumentMapper.mapper.toFindJson(documentService.get(documentId)));
    }
}