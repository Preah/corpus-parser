package kr.go.korean.corpus.parser.config;

import kr.go.korean.corpus.parser.entity.User;
import kr.go.korean.corpus.parser.entity.UserPrincipal;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class PrincipalHelper {

    public static User getUser(){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if(null == authentication) return null;

        if(false == authentication.isAuthenticated()) return null;

        if(authentication instanceof AnonymousAuthenticationToken) return null;

        return ((UserPrincipal)authentication.getPrincipal()).getUser();
    }


    public static Long getId(){
        User user = getUser();
        return null == user ? null : user.getId();
    }

    public static String getUserName(){
        User user = getUser();
        return null == user ? null : user.getUserName();
    }

    public static String getRealName(){
        User user = getUser();
        return null == user ? null : user.getRealName();
    }
}
