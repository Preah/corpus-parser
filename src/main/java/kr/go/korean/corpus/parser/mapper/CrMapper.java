package kr.go.korean.corpus.parser.mapper;


import kr.go.korean.corpus.parser.entity.Cr;
import kr.go.korean.corpus.parser.form.DocumentForm.Request;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Mapper(imports = {LocalDate.class, DateTimeFormatter.class})
public interface CrMapper {

    CrMapper mapper = Mappers.getMapper(CrMapper.class);


    List<Cr> toCrContext(@Context CrContext context, List<Request.CrConvert.Cr> form);
}

