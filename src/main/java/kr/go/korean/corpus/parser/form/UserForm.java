package kr.go.korean.corpus.parser.form;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotEmpty;

public class UserForm {


    public static class Request {

        @Getter
        @Setter
        @Builder(toBuilder = true)
        @ToString
        @NoArgsConstructor
        @AllArgsConstructor
        public static class changePassword {

            @Valid
            @NotEmpty(message = "비밀번호는 필수 입니다")
            @Length(min = 4, max = 20)
//            @Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})", message = "비밀 번호는 6~20자리로 숫자와 특수 문자가 포함된 영문 대소문자로 입력해 주세요")
            private String inputPassword;


            @Valid
            @NotEmpty(message = "확인 비밀번호는 필수 입니다")
            @Length(min = 4, max = 20)
            private String conformPassword;

            @Valid
            @AssertTrue(message = "비밀번호와 확인 비밀번호 정보는 동일 해야 됩니다.")
            private Boolean equalsPassword;

            @Valid
            @AssertTrue(message = "비밀번호와 확인 비밀번호 정보는 동일 해야 됩니다.")
            public boolean isValidPassword(){
                equalsPassword = inputPassword.equals(conformPassword);
                return inputPassword.equals(conformPassword);
            }
        }


        @Getter
        @Setter
        @Builder(toBuilder = true)
        @ToString
        @NoArgsConstructor
        @AllArgsConstructor
        public static class UserAdd {

            @Valid
            @NotEmpty
            @Length(min = 3, max = 30)
            private String userName;

            @Valid
            @NotEmpty
            @Length(min = 3, max = 30)
            private String realName;

            @Valid
            @Length(min = 0, max = 500)
            private String memo;
        }

    }

    public static class Response {

        @Data
        public static class FindAll {

            private Long id;
            private String userName;
            private String realName;
            private String memo;

            private Integer completeCount;
            private Integer progressCount;
        }
    }

}
