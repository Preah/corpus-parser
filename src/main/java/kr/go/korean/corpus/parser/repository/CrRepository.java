package kr.go.korean.corpus.parser.repository;

import kr.go.korean.corpus.parser.entity.Cr;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CrRepository extends JpaRepository <Cr, Long> {
}
