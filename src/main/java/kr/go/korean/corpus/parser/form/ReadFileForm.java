package kr.go.korean.corpus.parser.form;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ReadFileForm {

    @JsonProperty("id")
    private String id;


    @JsonProperty("metadata")
    private FileMetaData fileMetaData;

    @Getter
    @Setter
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class FileMetaData {

        @JsonProperty("title")
        private String title;

        @JsonProperty("author")
        private String author;

        @JsonProperty("publisher")
        private String publisher;

        @JsonProperty("year")
        private String year;

        @JsonProperty("annotation_level")
        private String[] annotationLevel;

        @JsonProperty("note")
        private String note;
    }

    @JsonProperty("document")
    private List<Document> document;

    @Getter
    @Setter
    @Builder
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Document {

        @JsonProperty("id")
        private String sort;


        @JsonProperty("metadata")
        private MetaData metaData;

        @Getter
        @Setter
        @Builder
        @ToString
        @NoArgsConstructor
        @AllArgsConstructor
        public static class MetaData {

            @JsonProperty("title")
            private String title;

            @JsonProperty("author")
            private String author;

            @JsonProperty("publisher")
            private String publisher;

            @JsonProperty("date")
            private String date;

            @JsonProperty("category")
            private String category;

            @JsonProperty("note")
            private String note;
        }

        @JsonProperty("sentence")
        private List<Sentence> sentences;

        @Getter
        @Setter
        @Builder
        @ToString
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Sentence {

            @JsonProperty("id")
            private String sort;

            @JsonProperty("form")
            private String content;

            @JsonProperty("word")
            private List<Word> words;

            @Getter
            @Setter
            @Builder
            @ToString
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Word {

                @JsonProperty("id")
                private Long sort;

                @JsonProperty("begin")
                private Integer start;

                @JsonProperty("end")
                private Integer finish;

                @JsonProperty("form")
                private String word;
            }


            @JsonProperty("NE")
            private List<Ne> nes;

            @Getter
            @Setter
            @Builder
            @ToString
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Ne {

                @JsonProperty("id")
                private Long sort;

                @JsonProperty("begin")
                private Integer start;

                @JsonProperty("end")
                private Integer finish;

                @JsonProperty("form")
                private String word;

                @JsonProperty("type")
                private String type;
            }
        }
    }
}
