package kr.go.korean.corpus.parser.mapper;

import kr.go.korean.corpus.parser.entity.Document;
import kr.go.korean.corpus.parser.entity.MetaData;
import kr.go.korean.corpus.parser.entity.Ne;
import kr.go.korean.corpus.parser.entity.Sentence;
import lombok.Builder;
import org.mapstruct.AfterMapping;
import org.mapstruct.BeforeMapping;
import org.mapstruct.MappingTarget;

@Builder
public class DocumentContext {

    private Document document;
    private MetaData metaData;
    private Sentence sentence;
    private Ne ne;

    @BeforeMapping
    public void initDocument(@MappingTarget Document target){
        this.document = target;
    }

    @BeforeMapping
    public void initMetaData(@MappingTarget MetaData target){
        this.metaData = target;
    }

    @BeforeMapping
    public void initSentence(@MappingTarget Sentence target){
        this.sentence = target;
    }

    @BeforeMapping
    public void initNe(@MappingTarget Ne target){
        this.ne = target;
    }

    @AfterMapping
    public void setDocument(@MappingTarget Document target){
        this.document = target;
        this.document.setStatus(false);
        target.setId(null);
    }

    @AfterMapping
    public void setMetaData(@MappingTarget MetaData target){
        target.setDocument(this.document);
        target.setId(null);
    }

    @AfterMapping
    public void setSentence(@MappingTarget Sentence target){
        target.setDocument(this.document);
        target.setId(null);
    }

    @AfterMapping
    public void setNe(@MappingTarget Ne target){
        target.setSentence(this.sentence);
        target.setId(null);
    }
}
