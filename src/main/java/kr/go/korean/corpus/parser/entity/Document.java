package kr.go.korean.corpus.parser.entity;

import kr.go.korean.corpus.parser.code.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(toBuilder = true)
@Entity
@Table(name="tb_document")
@SequenceGenerator(
        name="seq_document",
        sequenceName="seq_document",
        allocationSize=1
)
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_document")
    @Column(name = "document_id")
    private Long id;

    @Column(name="sort", length = 50)
    private String sort;

    @Column(name="status")
    private Boolean status;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document")
    private List<Sentence> sentences;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document", orphanRemoval = true)
    private List<Cr> crs;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document", orphanRemoval = true)
    private MetaData metaData;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
