package kr.go.korean.corpus.parser.controller;

import kr.go.korean.corpus.parser.config.PrincipalHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {


    @GetMapping("/")
    public String index(Model model) {

        System.out.println(PrincipalHelper.getUser().getId());

        if(!PrincipalHelper.getUser().getResetPassword()){

            return "redirect:/login/password/change";
        }
        return "redirect:/document/list";
    }
}
