package kr.go.korean.corpus.parser.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(toBuilder = true)
@Entity
@Table(name="tb_ne")
@SequenceGenerator(
        name="seq_ne",
        sequenceName="seq_ne",
        allocationSize=1
)
public class Ne {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_ne")
    @Column(name = "ne_id")
    private Long id;

    @Column(name="sort")
    private Integer sort;

    @Column(name="start")
    private Integer start;

    @Column(name="finish")
    private Integer finish;

    @Column(name="word", length = 1000)
    private String word;

    @Column(name="type", length = 10)
    private String type;

    @ManyToOne
    @JoinColumn(name = "sentence_id")
    private Sentence sentence;
}
