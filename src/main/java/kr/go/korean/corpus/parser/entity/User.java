package kr.go.korean.corpus.parser.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(toBuilder = true)
@Entity
@Table(name="tb_user")
@SequenceGenerator(
        name="seq_user",
        sequenceName="seq_user",
        allocationSize=1
)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_user")
    @Column(name = "user_id")
    private Long id;

    @Column(unique = true, name="user_name", length = 30)
    private String userName;

    @Column(name="password", length = 200)
    private String password;

    @Column(name="real_name", length = 30)
    private String realName;

    @Column(name="reset_password")
    private Boolean resetPassword;

    @Column(name="memo", length = 4000)
    private String memo;


    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user", orphanRemoval = true)
    private List<Document> documents;


}
