package kr.go.korean.corpus.parser.service;

import kr.go.korean.corpus.parser.config.PrincipalHelper;
import kr.go.korean.corpus.parser.config.SecurityConfig;
import kr.go.korean.corpus.parser.entity.Document;
import kr.go.korean.corpus.parser.entity.User;
import kr.go.korean.corpus.parser.entity.UserPrincipal;
import kr.go.korean.corpus.parser.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    SecurityConfig securityConfig;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        User findUser = userRepository.findByUserName(userName);

        if(findUser == null)
            throw new UsernameNotFoundException("User 404");

        return new UserPrincipal(findUser);
    }


    public List<User> getAll(){
        return userRepository.findByUserNameNot("admin");
    }


    public User add(User user){

        PasswordEncoder passwordEncoder = securityConfig.passwordEncoder();
        return userRepository.save(user.toBuilder()
                                        .resetPassword(false)
                                        .password(passwordEncoder.encode("1234"))
                                        .build());
    }

    public User changePassword(User user){

        User findUser = userRepository.getOne(PrincipalHelper.getId());
        PasswordEncoder passwordEncoder = securityConfig.passwordEncoder();
        findUser.setPassword(passwordEncoder.encode(user.getPassword()));
        findUser.setResetPassword(true);
        return userRepository.save(findUser);
    }

    public User addDocuments(Long userId, List<Document> documents){
        User findUser = userRepository.findById(userId).get();
        findUser.getDocuments().clear();
        findUser.getDocuments().addAll(documents);

        documents.stream().forEach(document -> {
            document.setUser(findUser);
        });


        return userRepository.save(findUser);
    }

}
