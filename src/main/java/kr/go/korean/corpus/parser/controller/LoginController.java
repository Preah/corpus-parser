package kr.go.korean.corpus.parser.controller;

import kr.go.korean.corpus.parser.entity.User;
import kr.go.korean.corpus.parser.form.UserForm.Request;
import kr.go.korean.corpus.parser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class LoginController {

    @Autowired
    UserService userService;


    @GetMapping("/login")
    public String login(Model model){
        return "login/login";
    }


    @GetMapping("/login/password/change")
    public String passwordChange(Model model, @ModelAttribute("changePassword") Request.changePassword password){

        model.addAttribute("changePassword", Request.changePassword.builder().build());
        return "login/password-change";
    }

    @PostMapping("/login/password/modify")
    public String passwordModify(Model model, @Valid @ModelAttribute("changePassword") Request.changePassword password, Errors errors){

        if(errors.hasErrors()){
            model.addAttribute("changePassword", password);
            return "login/password-change";
        }else{
            userService.changePassword(User.builder().password(password.getInputPassword()).build());
            return "redirect:/document/list";
        }
    }
}
