package kr.go.korean.corpus.parser.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.go.korean.corpus.parser.entity.Document;
import kr.go.korean.corpus.parser.entity.User;
import kr.go.korean.corpus.parser.form.ReadFileForm;
import kr.go.korean.corpus.parser.form.UserForm.Request;
import kr.go.korean.corpus.parser.mapper.DocumentContext;
import kr.go.korean.corpus.parser.mapper.DocumentMapper;
import kr.go.korean.corpus.parser.service.DocumentService;
import kr.go.korean.corpus.parser.service.UserService;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.apache.commons.io.*;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static kr.go.korean.corpus.parser.mapper.UserMapper.mapper;

@Controller
public class SystemController {


    @Autowired
    UserService userService;

    @Autowired
    DocumentService documentService;

    @Autowired
    ResourceLoader resourceLoader;


    @GetMapping("/system/user/create")
    public String userCreate(Model model) {

        model.addAttribute("user", User.builder().build());
        model.addAttribute("users", mapper.toFindAll(userService.getAll()));
        return "/system/user-create";
    }

    @PostMapping("/system/user/reg")
    public String userReg(Model model, @Valid @ModelAttribute("user") Request.UserAdd user, Errors errors) {

        if(errors.hasErrors()){
            model.addAttribute("user", user);
            model.addAttribute("users", mapper.toFindAll(userService.getAll()));
            return "/system/user-create";
        }else{
            userService.add(mapper.toUser(user));
            return "redirect:/system/user/create";
        }
    }


    @GetMapping("/system/document/reg")
    public String documentReg(Model model, @Param("userId") Long userId) throws IOException {

        List<Document> documents = new ArrayList<>();
        File file = new File(resourceLoader.getResource("classpath:/json").getURI().getPath());

        if(file.isDirectory()){
            List<File> jsonFiles = (List<File>) FileUtils.listFiles(file, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);

            jsonFiles.forEach(jsonFile -> {
                // read JSON and load json
                ObjectMapper jsonMapper = new ObjectMapper().configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
                TypeReference<ReadFileForm> typeReference = new TypeReference<ReadFileForm>(){};
                InputStream inputStream = TypeReference.class.getResourceAsStream("/json/"+jsonFile.getName());
                ReadFileForm readFileForm = ReadFileForm.builder().build();
                try {
                    readFileForm = jsonMapper.readValue(inputStream, typeReference);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                readFileForm.getDocument().stream().forEach(documentForm -> {
                    documents.add(DocumentMapper.mapper.toDocumentContext(DocumentContext.builder().build(), documentForm));
                });

            /*
            readFileForm.getFile().stream()
                                  .flatMap(file -> file.getDocument().stream()).collect(Collectors.toList())
                                  .forEach(documentForm -> {
                                      documents.add(mapper.toDocument(DocumentContext.builder().build(), documentForm));
            });

           */
            });
        }
        userService.addDocuments(userId, documents);
        return "redirect:/system/user/create";
    }
}
