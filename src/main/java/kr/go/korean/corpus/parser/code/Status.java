package kr.go.korean.corpus.parser.code;


public enum Status {

      COMPLETE  // 완료
    , PROGRESS; // 진행중

    public String toValue() {
        switch (this) {
            case COMPLETE : return "완료";
            case PROGRESS : return "진행중";
        }
        return null;
    }
}
