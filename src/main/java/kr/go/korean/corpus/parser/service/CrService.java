package kr.go.korean.corpus.parser.service;

import kr.go.korean.corpus.parser.code.Status;
import kr.go.korean.corpus.parser.entity.Cr;
import kr.go.korean.corpus.parser.entity.Document;
import kr.go.korean.corpus.parser.repository.CrRepository;
import kr.go.korean.corpus.parser.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CrService {

    @Autowired
    CrRepository crRepository;

    @Autowired
    DocumentRepository documentRepository;

    public Cr addList(List<Cr> crs, Long documentId, Boolean status){

        Document findDocument = documentRepository.getOne(documentId);
        findDocument.setStatus(status);

        findDocument.getCrs().clear();
        crs.forEach(cr -> {
            cr.setDocument(findDocument);
        });

        crRepository.saveAll(crs);
        documentRepository.save(findDocument);
        return Cr.builder().build();
    }
}
