package kr.go.korean.corpus.parser.service;

import kr.go.korean.corpus.parser.entity.Sentence;
import kr.go.korean.corpus.parser.repository.SentenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SentenceService {

    @Autowired
    SentenceRepository sentenceRepository;

    @Transactional(readOnly = true)
    public Sentence get(Long sentenceId){
        return sentenceRepository.getOne(sentenceId);
    }
}
