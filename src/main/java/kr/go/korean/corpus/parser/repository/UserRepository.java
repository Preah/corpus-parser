package kr.go.korean.corpus.parser.repository;

import kr.go.korean.corpus.parser.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository <User, Long> {


    User findByUserName(String userName);

    List<User> findByUserNameNot(String userName);
}
