package kr.go.korean.corpus.parser.mapper;


import kr.go.korean.corpus.parser.entity.*;
import kr.go.korean.corpus.parser.form.DocumentForm.Response;
import kr.go.korean.corpus.parser.form.ReadFileForm;
import org.hibernate.mapping.Collection;
import org.mapstruct.*;
import org.mapstruct.Context;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Mapper(imports = {LocalDate.class, DateTimeFormatter.class})
public interface DocumentMapper {

    DocumentMapper mapper = Mappers.getMapper(DocumentMapper.class);

    Document toDocumentContext(@Context DocumentContext context, ReadFileForm.Document form);

    @Mappings({
            @Mapping(target = "neTotalCount", source = "entity", qualifiedByName = "toNeTotalCount"),
            @Mapping(target = "crTotalCount", source = "entity", qualifiedByName = "toCrTotalCount")
    })
    Response.FindOne toFindOne(Document entity);




    @Mapping(target = "contentReplace", source = "entity", qualifiedByName = "toContentReplace")
    Response.FindOne.Sentence toSentence(Sentence entity);
    List<Response.FindOne.Sentence> toSentences(List<Sentence> entity);


    Response.FindOne.Sentence.Ne toNe(Ne entity);
    List<Response.FindOne.Sentence.Ne> toNes(List<Ne> entity);


    Response.FindOne.Cr toCr(Cr entity);
    List<Response.FindOne.Cr> toCrs(List<Cr> entity);

    Response.FindOne.Cr.CrEntity toCrEntity(CrEntity entity);
    List<Response.FindOne.Cr.CrEntity> toCrEntities(List<CrEntity> entity);



    @Mappings({
            @Mapping(target = "neTotalCount", source = "entity", qualifiedByName = "toNeTotalCount"),
            @Mapping(target = "crTotalCount", source = "entity", qualifiedByName = "toCrTotalCount")
    })
    Response.FindAll toFindAll(Document entity);

    Response.FindJson toFindJson(Document entity);



    @Named("toNeTotalCount")
    default Integer toNeTotalCount(Document document) {
        return document.getSentences().stream()
                .flatMap(sentence -> sentence.getNes().stream())
                .collect(Collectors.toList())
                .size();
    }

    @Named("toCrTotalCount")
    default Integer toCrTotalCount(Document document) {
        return document.getCrs().stream()
                .flatMap(cr -> cr.getCrEntities().stream())
                .collect(Collectors.toList())
                .size();
    }



    @Named("toContentReplace")
    default String toContentReplace(Sentence sentence) {

        String[] array = sentence.getContent().split("");

        String resultStr = "";

        List<Ne> neList = new ArrayList<Ne>();

        neList.addAll(
          Optional.ofNullable(sentence.getDocument().getCrs()
                  .stream()
                  .flatMap(cr -> cr.getCrEntities()
                                   .stream()
                                   .filter(e -> e.getSentenceSort() == sentence.getSort() && e.getNeSort() == -1))
                                   .map(crEntity -> Ne.builder()
                                                      .start(crEntity.getStart())
                                                      .finish(crEntity.getFinish())
                                                      .sort(crEntity.getNeSort())
                                                      .build()).collect(Collectors.toList())
                  ).orElse(Collections.emptyList())
        );

        neList.addAll(
          Optional.ofNullable(sentence.getNes()
                  .stream()
                  .map(ne -> Ne.builder()
                          .start(ne.getStart())
                          .finish(ne.getFinish())
                          .sort(ne.getSort())
                          .build())
                  .collect(Collectors.toList())
          ).orElse(Collections.emptyList())
        );

        neList = neList.stream().sorted(Comparator.comparing(Ne::getStart)).collect(Collectors.toList());

        int[] startPoint  = neList.stream().mapToInt(Ne::getStart).toArray();
        int[] finishPoint = neList.stream().mapToInt(Ne::getFinish).toArray();


        for (int i = 0; i < array.length; i++) {

            for (int begin : startPoint) {
                if (i == begin - 1) {
                    Ne ne = neList.stream().filter(n -> n.getStart() == begin).findFirst().get();

                    resultStr += "<span style=\'cursor:pointer;text-decoration:underline;\' name=\'neItem\' class=\'text-primary\'";
                    resultStr += " onclick=\'funcNeSpanOnClick(this)\' data-css-name=\'\'";
                    resultStr += " data-sentence-sort=\'" + sentence.getSort() + "\' data-ne-sort=\'" + ne.getSort() + "\'";
                    resultStr += " data-start=\'" + ne.getStart() + "\' data-finish=\'" + ne.getFinish() + "\'";
                    resultStr += ">";

                }
            }

            resultStr += array[i];

            for (int end : finishPoint) {
                if (i == end - 1) {
                    resultStr += "</span>";
                }
            }
        }

        return resultStr;
    }

}
