package kr.go.korean.corpus.parser.predicate;

import com.querydsl.core.BooleanBuilder;
import kr.go.korean.corpus.parser.config.PrincipalHelper;
import kr.go.korean.corpus.parser.entity.QDocument;
import kr.go.korean.corpus.parser.form.DocumentForm.*;
import com.querydsl.core.types.Predicate;

import java.util.Optional;

public class DocumentPredicate {

    public static Predicate getPage(Request.FindAll findAll){

        QDocument document = QDocument.document;
        BooleanBuilder builder = new BooleanBuilder();

        Optional.ofNullable(findAll.getStatus()).ifPresent(p -> builder.and(document.status.eq(p)));
        Optional.ofNullable(findAll.getMetaData()).ifPresent(p -> builder.and(document.metaData.title.contains(p.getTitle())));
        builder.and(document.user.id.eq(PrincipalHelper.getId()));
        return builder;
    }
}
