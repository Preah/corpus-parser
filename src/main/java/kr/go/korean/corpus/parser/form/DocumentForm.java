package kr.go.korean.corpus.parser.form;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

public class DocumentForm {

    public static class Request {

        @Getter
        @Setter
        @Builder(toBuilder = true)
        @ToString
        @NoArgsConstructor
        @AllArgsConstructor
        public static class FindAll {

            private Long id;
            private String sort;
            private String subClass;
            private Boolean status;
//            private String firstSentence;
            private MetaData metaData;

            @Data
            public static class MetaData {

                private String title;
                private String category;
            }
        }

        @Getter
        @Setter
        @Builder(toBuilder = true)
        @ToString
        @NoArgsConstructor
        @AllArgsConstructor
        public static class FindOne {

            @Valid
            @NotNull
            private Long id;
        }


        @Getter
        @Setter
        @Builder(toBuilder = true)
        @ToString
        @NoArgsConstructor
        @AllArgsConstructor
        public static class CrConvert {

            @JsonProperty("cr")
            private List<Cr> crs;

            @Getter
            @Setter
            @Builder(toBuilder = true)
            @ToString
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Cr {

                @JsonProperty("entity")
                List<CrEntity> crEntities;

                @Getter
                @Setter
                @Builder(toBuilder = true)
                @ToString
                @NoArgsConstructor
                @AllArgsConstructor
                public static class CrEntity {

                    @JsonProperty("cssName")
                    private String cssName;

                    @JsonProperty("sentenceSort")
                    private String sentenceSort;

                    @JsonProperty("neSort")
                    private Integer neSort;

                    @JsonProperty("start")
                    private Integer start;

                    @JsonProperty("finish")
                    private Integer finish;

                    @JsonProperty("word")
                    private String word;
                }
            }
        }
    }


    public static class Response {

        @Data
        public static class FindAll {

            private Long id;
            private String sort;
            private String subClass;
            private Boolean status;
            private Integer neTotalCount;
            private Integer crTotalCount;

            private MetaData metaData;

            @Data
            public static class MetaData {

                private String title;
                private String category;
            }
        }

        @Data
        public static class FindOne {

            private Long id;
            private String sort;
            private Boolean status;
            private Integer neTotalCount;
            private Integer crTotalCount;
            private MetaData metaData;

            @Data
            public static class MetaData {

                private String title;
                private String category;
            }

            private List<Sentence> sentences;

            @Data
            public static class Sentence {

                private String sort;
                private String content;
                private String contentReplace;

                private List<Ne> nes;

                @Data
                public static class Ne {

                    @JsonProperty("id")
                    private Integer sort;

                    @JsonProperty("begin")
                    private Integer start;

                    @JsonProperty("end")
                    private Integer finish;

                    @JsonProperty("text")
                    private String word;

                    @JsonProperty("type")
                    private String type;
                }
            }

            private List<Cr> crs;

            @Data
            public static class Cr {

                List<CrEntity> crEntities;

                @Data
                public static class CrEntity {

                    private String word;
                    private String sentenceSort;
                    private Integer neSort;
                    private Integer start;
                    private Integer finish;
                }
            }
        }

        @Data
        public static class FindJson {

            @JsonProperty("id")
            private String sort;

            @JsonProperty("metadata")
            private MetaData metaData;

            @Data
            public static class MetaData {

                private String title;
                private String author;
                private String publisher;
                private String date;
                private String category;
                private String note;
            }

            @JsonProperty("sentence")
            private List<Sentence> sentences;

            @Data
            public static class Sentence {

                @JsonProperty("id")
                private String sort;

                @JsonProperty("form")
                private String content;

                @JsonProperty("NE")
                private List<Ne> nes;

                @Data
                public static class Ne {

                    @JsonProperty("id")
                    private Integer sort;

                    @JsonProperty("form")
                    private String word;

                    @JsonProperty("type")
                    private String type;

                    @JsonProperty("begin")
                    private Integer start;

                    @JsonProperty("end")
                    private Integer finish;
                }
            }


            @JsonProperty("CR")
            private List<Cr> crs;

            @Data
            public static class Cr {

                @JsonProperty("entity")
                List<CrEntity> crEntities;

                @Data
                public static class CrEntity {

                    @JsonProperty("form")
                    private String word;

                    @JsonProperty("NE_id")
                    private Integer neSort;

                    @JsonProperty("sentence_id")
                    private String sentenceSort;

                    @JsonProperty("begin")
                    private Integer start;

                    @JsonProperty("end")
                    private Integer finish;
                }
            }
        }
    }
}
