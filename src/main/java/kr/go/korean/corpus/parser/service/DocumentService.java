package kr.go.korean.corpus.parser.service;

import com.querydsl.core.types.Predicate;
import kr.go.korean.corpus.parser.entity.Document;
import kr.go.korean.corpus.parser.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DocumentService {

    @Autowired
    DocumentRepository documentRepository;

    @Transactional(readOnly = true)
    public Document get(Long documentId){
        return documentRepository.findById(documentId).get();
    }

    @Transactional(readOnly = true)
    public List<Document> getList(){
        return documentRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Page<Document> getPage(Predicate predicate, Pageable pageable){
        return documentRepository.findAll(predicate, pageable);
    }
}
