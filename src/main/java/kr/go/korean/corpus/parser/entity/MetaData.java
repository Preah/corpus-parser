package kr.go.korean.corpus.parser.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(toBuilder = true)
@Entity
@Table(name="tb_metadata")
@SequenceGenerator(
        name="seq_metadata",
        sequenceName="seq_metadata",
        allocationSize=1
)
public class MetaData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_metadata")
    @Column(name = "metadata_id")
    private Long id;

    @Column(name="title", length = 100)
    private String title;

    @Column(name="author", length = 100)
    private String author;

    @Column(name="publisher", length = 100)
    private String publisher;

    @Column(name="meta_date", length = 100)
    private String date;

    @Column(name="category", length = 100)
    private String category;

    @Column(name="note", length = 100)
    private String note;

    @OneToOne
    @JoinColumn(name = "document_id")
    private Document document;

}
