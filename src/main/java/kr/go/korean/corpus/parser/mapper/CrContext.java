package kr.go.korean.corpus.parser.mapper;

import kr.go.korean.corpus.parser.entity.*;
import lombok.Builder;
import org.mapstruct.AfterMapping;
import org.mapstruct.BeforeMapping;
import org.mapstruct.MappingTarget;

@Builder
public class CrContext {

    private Cr cr;
    private CrEntity crEntity;

    @BeforeMapping
    public void initCr(@MappingTarget Cr target){
        this.cr = target;
    }

    @BeforeMapping
    public void initCrEntity(@MappingTarget CrEntity target){
        this.crEntity = target;
    }

    @AfterMapping
    public void setCr(@MappingTarget Cr target){
        target.setId(null);
    }

    @AfterMapping
    public void setCrEntity(@MappingTarget CrEntity target){
        target.setCr(this.cr);
        target.setId(null);
    }
}
