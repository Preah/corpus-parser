package kr.go.korean.corpus.parser.repository;

import kr.go.korean.corpus.parser.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface DocumentRepository extends JpaRepository <Document, Long>, QuerydslPredicateExecutor<Document> {
}
