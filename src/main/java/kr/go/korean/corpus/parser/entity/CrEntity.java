package kr.go.korean.corpus.parser.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(toBuilder = true)
@Entity
@Table(name="tb_entity")
@SequenceGenerator(
        name="seq_entity",
        sequenceName="seq_entity",
        allocationSize=1
)
public class CrEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_entity")
    @Column(name = "cr_entity_id")
    private Long id;

    @Column(name = "sentence_sort", length = 50)
    private String sentenceSort;

    @Column(name = "ne_sort")
    private Integer neSort;

    @Column(name = "word", length = 1000)
    private String word;

    @Column(name="start")
    private Integer start;

    @Column(name="finish")
    private Integer finish;

    @ManyToOne
    @JoinColumn(name = "cr_id")
    private Cr cr;
}
